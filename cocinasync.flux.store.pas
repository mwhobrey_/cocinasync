unit cocinasync.flux.store;

interface

uses System.SysUtils, System.Classes, System.Generics.Collections;

type
  TStoreID = string;

  TBaseStore = class(TObject)
  private
    FViews : TDictionary<TObject, TProc<TBaseStore>>;
  public
    constructor Create; virtual;
    destructor Destroy; override;

    procedure RegisterForUpdates<SC : TBaseStore>(Context : TObject; Handler : TProc<SC>);
    procedure UnregisterForUpdates(Context : TObject);
    procedure UpdateViews; virtual;
  end;
  TBaseStoreClass = class of TBaseStore;

  TBaseDataModuleStore = class(TDataModule)
  private
    FBaseStore: TBaseStore;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure UpdateViews; virtual;
    property BaseStore : TBaseStore read FBaseStore;
    procedure RegisterForUpdates<SC : TBaseDataModuleStore>(Context : TObject; Handler : TProc<SC>);
    procedure UnregisterForUpdates(Context : TObject);
  end;

implementation

uses cocinasync.async;

{ TBaseDataModuleStore }

constructor TBaseDataModuleStore.Create(AOwner: TComponent);
begin
  inherited;
  FBaseStore := TBaseStore.Create;
end;

destructor TBaseDataModuleStore.Destroy;
begin
  FBaseStore.Free;
  inherited;
end;

procedure TBaseDataModuleStore.RegisterForUpdates<SC>(Context : TObject; Handler: TProc<SC>);
begin
  FBaseStore.RegisterForUpdates<TBaseStore>(Context,
    procedure(Store : TBaseStore)
    begin
      Handler(Self);
    end
  );
end;

procedure TBaseDataModuleStore.UnregisterForUpdates(Context: TObject);
begin
  FBaseStore.UnregisterForUpdates(Context);
end;

procedure TBaseDataModuleStore.UpdateViews;
begin
  FBaseStore.UpdateViews;
end;

{ TBaseStore }

constructor TBaseStore.Create;
begin
  inherited Create;
  FViews := TDictionary<TObject, TProc<TBaseStore>>.Create;
end;

destructor TBaseStore.Destroy;
begin
  FViews.Free;
  inherited;
end;

procedure TBaseStore.RegisterForUpdates<SC>(Context : TObject; Handler: TProc<SC>);
begin
  TMonitor.Enter(FViews);
  try
    FViews.AddOrSetValue(Context,
      procedure(Store : TBaseStore)
      begin
        Handler(Store);
      end
    );
  finally
    TMonitor.Exit(FViews);
  end;
end;

procedure TBaseStore.UnregisterForUpdates(Context: TObject);
begin
  TMonitor.Enter(FViews);
  try
    FViews.Remove(Context);
  finally
    TMonitor.Exit(FViews);
  end;
end;

procedure TBaseStore.UpdateViews;
var
  ary : TArray<TPair<TObject, TProc<TBaseStore>>>;
  a: TPair<TObject, TProc<TBaseStore>>;
begin
  TMonitor.Enter(FViews);
  try
    ary := FViews.ToArray;
  finally
    TMonitor.Exit(FViews);
  end;
  for a in ary do
  begin
    TAsync.QueueIfInThread(
      procedure
      begin
        a.Value(Self);
      end
    );
  end;
end;

end.
