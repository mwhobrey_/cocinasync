unit fluxdemo.stores.history;

interface

uses System.Classes, System.SysUtils, cocinasync.flux.store;

type

  THistoryStore = class(TBaseStore)
  private
    FItems: TStrings;
    procedure LogAction(Action : TObject);
  public
    constructor Create; override;
    destructor Destroy; override;

    property Items : TStrings read FItems;
  end;

var
  HistoryStore : THistoryStore;

implementation

uses
  cocinasync.flux.dispatcher, fluxdemo.actions.todoitem;

{ THistoryStore }

constructor THistoryStore.Create;
begin
  inherited;
  FItems := TStringList.Create;

  Flux.Register<TNewItemAction>(Self,
    procedure(Action : TNewItemAction)
    begin
      LogAction(Action);
    end
  );

  Flux.Register<TDeleteItemAction>(Self,
    procedure(Action : TDeleteItemAction)
    begin
      LogAction(Action);
    end
  );

  Flux.Register<TToggleItemAction>(Self,
    procedure(Action : TToggleItemAction)
    begin
      LogAction(Action);
    end
  );
end;

destructor THistoryStore.Destroy;
begin
  FItems.Free;
  inherited;
end;

procedure THistoryStore.LogAction(Action : TObject);
var
  sDate : string;
begin
  DateTimeToString(sDate, 'yyyy-mm-dd hh:nn:ss', Now);
  FItems.Add(sDate+' '+Action.ToString);
end;

initialization
  HistoryStore := THistoryStore.Create;

finalization
  HistoryStore.Free;

end.
