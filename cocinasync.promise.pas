unit cocinasync.promise;

interface

uses
  System.SysUtils;

type
  IPromise = interface
    function All(const ary : TArray<TProc>) : IPromise;
    function Resolve(Proc : TProc) : IPromise;
    function &And(Proc : TProc) : IPromise;
    function &Then(Proc : TProc) : IPromise;
    procedure Done;
  end;

  function Promise(Proc : TProc) : IPromise;

implementation

uses
  System.SyncObjs,
  System.Generics.Collections,
  System.Classes,
  Cocinasync.collections,
  Cocinasync.jobs;


type
  TProc = System.SysUtils.TProc;
  TPromise = class(TInterfacedObject, IPromise)
    type
      TPromiseThread = class(TThread)
      private
        FCount : integer;
        FCurrentJobs : TQueue<TProc>;
        FDone : Boolean;
        FNextJobs : TQueue<TProc>;
        FQueues : TQueue<TQueue<TProc>>;
        FPromise : TPromise;
      protected
        procedure Execute; override;
      public
        procedure AddJob(const Proc : TProc; Wait : boolean);
        constructor Create(Promise : TPromise);
        destructor Destroy; override;
      end;
  private
    FThread : TPromiseThread;
  public
    function All(const ary : TArray<TProc>) : IPromise;
    function Resolve(Proc : TProc) : IPromise;
    function &And(Proc : TProc) : IPromise;
    function &Then(Proc : TProc) : IPromise;
    procedure Done;

    constructor Create;
    destructor Destroy; override;
  end;

function Promise(Proc : TProc) : IPromise; overload;
begin
  Result := TPromise.Create;
  Result.Resolve(Proc);
end;

{ TPromise }

constructor TPromise.Create;
begin
  inherited Create;
  FThread := TPromiseThread.Create(Self);
end;

destructor TPromise.Destroy;
begin
  FThread.Terminate;
  inherited;
end;

procedure TPromise.Done;
begin
  FThread.FDone := True;
  _AddRef;
end;

function TPromise.&And(Proc: TProc): IPromise;
begin
  FThread.AddJob(Proc, False);
  Result := Self;
end;

function TPromise.All(const ary: TArray<TProc>): IPromise;
var
  p : TProc;
begin
  for p in ary do
  begin
    FThread.AddJob(p, False);
  end;
  result := Self;
end;

function TPromise.Resolve(Proc: TProc): IPromise;
begin
  FThread.AddJob(Proc, False);
  Result := Self;
end;

function TPromise.&Then(Proc: TProc): IPromise;
begin
  FThread.AddJob(Proc, True);
  Result := Self;
end;

{ TPromise.TPromiseThread }

procedure TPromise.TPromiseThread.Execute;
var
  p : TProc;
begin
  repeat
    if FCurrentJobs.Count > 0 then
    begin
      TInterlocked.Increment(FCount);
      p := FCurrentJobs.Dequeue();
      TJobManager.Execute(
        procedure
        begin
          p();
          TInterlocked.Decrement(FCount);
        end
      );
    end else
    begin
      if (FCount = 0) and (FQueues.Count > 0) then
      begin
        TMonitor.Enter(FQueues);
        try
          FCurrentJobs := FQueues.Dequeue;
        finally
          TMonitor.Exit(FQueues);
        end;
      end;
      if (FCount = 0) and (FQueues.Count = 0) and (FCurrentJobs.Count = 0) and FDone then
        Terminate;
    end;

    sleep(1);
  until Terminated;
  FPromise._Release;
end;

procedure TPromise.TPromiseThread.AddJob(const Proc: TProc; Wait : boolean);
begin
  if Wait then
  begin
    TMonitor.Enter(FQueues);
    try
      FNextJobs := TQueue<TProc>.Create(1024);
      FQueues.Enqueue(FNextJobs);
      FNextJobs.Enqueue(Proc);
    finally
      TMonitor.Exit(FQueues);
    end;
  end else
    FNextJobs.Enqueue(Proc)
end;

constructor TPromise.TPromiseThread.Create(Promise : TPromise);
begin
  inherited Create(False);
  FCount := 0;
  FDone := False;
  FPromise := Promise;
  FreeOnTerminate := True;
  FQueues := TQueue<TQueue<TProc>>.Create(1024);
  FCurrentJobs := TQueue<TProc>.Create(1024);
  FNextJobs := FCurrentJobs;
end;

destructor TPromise.TPromiseThread.Destroy;
var
  q : TQueue<TProc>;
begin
  repeat
    q := FQueues.Dequeue;
    if not Assigned(q) then
      q.Free;
  until not Assigned(q);
  FCurrentJobs.Free;
  inherited;
end;

end.
